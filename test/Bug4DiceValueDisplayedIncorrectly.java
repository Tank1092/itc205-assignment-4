import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class Bug4DiceValueDisplayedIncorrectly {

    @Test
    public void displayedDiceValues_ShouldHave_CorrectCapitalisation() {
        // Arrange
        String expectedOutput = "Crown Anchor Diamond Club Spade Heart";
        
        DiceValue crown = DiceValue.CROWN;
        DiceValue anchor = DiceValue.ANCHOR;
        DiceValue diamond = DiceValue.DIAMOND;
        DiceValue club = DiceValue.CLUB;
        DiceValue spade = DiceValue.SPADE;
        DiceValue heart = DiceValue.HEART;
        
        PrintStream original = System.out;
        OutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);
        
        // Execute
        System.out.printf("%s %s %s %s %s %s", crown, anchor, diamond, club, spade, heart);
        
        // Assert
        assertThat(outputStream.toString(), containsString(expectedOutput));
        System.setOut(original);
    }

}
