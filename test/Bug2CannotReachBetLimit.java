import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class Bug2CannotReachBetLimit {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void balanceExceedsLimitBy_ShouldReturnTrueWhen_BalanceEqualsBet() {
        // Arrange
        int balance = 5;
        int bet = 5;
        int limit = 0;
        Player player = new Player("Fred", balance);
        player.setLimit(limit);
        boolean expectedReturnValue = true;
        
        // Execute
        boolean actualReturnValue = player.balanceExceedsLimitBy(bet);
        
        // Assert
        assertThat(actualReturnValue, equalTo(expectedReturnValue));
    }
    
    
    @Test
    public void takeBet_ShouldNotThrowExceptionWhen_BalanceEqualsBet() {
        // Arrange
        int balance = 5;
        int bet = 5;
        int limit = 0;
        Player player = new Player("Fred", balance);
        player.setLimit(limit);
        
        // Execute
        try {
            player.takeBet(bet);
        } catch (IllegalArgumentException exception){
            // Assert
            fail("Exception should have not have been thrown");
        }
    }

}
