import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Bug3IncorrectOdds {
    
    @Test
    public void oddsOfWinning_ShouldBe_ExpectedValue() {
        // Arrange
        Dice dice1 = new Dice();
        Dice dice2 = new Dice();
        Dice dice3 = new Dice();
        Player player = new Player("Fred", 300_000);
        DiceValue randomPick;
        int bet = 1;
        Game sut = new Game(dice1, dice2, dice3);
        int winnings = 0;
        int roundsPlayed = 0;
        int roundsWon = 0;

        // Execute
        for (/*blank*/; roundsPlayed < 100_000; roundsPlayed++) {
            randomPick = DiceValue.getRandom();
            winnings = sut.playRound(player, randomPick, bet);

            if (winnings > 0) {
                roundsWon++;
            } 
        }
        double ratio = (double) roundsWon / (double) (roundsPlayed);
        
        // Assert
        assertEquals(0.42, ratio, 0.01);
    }
    
    
    @Test
    public void playingRoundShouldRandomizeDiceValues() {
        // Arrange
        Dice dice1 = new Dice();
        Dice dice2 = new Dice();
        Dice dice3 = new Dice();
        Player player = new Player("Fred", 100);
        DiceValue randomPick = DiceValue.getRandom();
        int bet = 0;
        Game sut = new Game(dice1, dice2, dice3);
        DiceValue initialDice1Value;
        DiceValue initialDice2Value;
        DiceValue initialDice3Value;
        int unchangedCount = 0;
        int roundsPlayed = 0;
        
        // Execute
        for (/*blank*/; roundsPlayed < 100; roundsPlayed++) {
            initialDice1Value = dice1.getValue();
            initialDice2Value = dice2.getValue();
            initialDice3Value = dice3.getValue();
            
            sut.playRound(player, randomPick, bet);
            
            boolean dice1Unchanged = initialDice1Value == dice1.getValue();
            boolean dice2Unchanged = initialDice2Value == dice2.getValue();
            boolean dice3Unchanged = initialDice3Value == dice3.getValue();
            
            if (dice1Unchanged && dice2Unchanged && dice3Unchanged){
                unchangedCount++;
            }
        }
        
        // Assert
        if (roundsPlayed == 100 && unchangedCount >= 100) {
            fail("After 100 rounds the dice values never changed");
        }
    }
    
    
    @Test
    public void allPossibleDiceValues_ShouldBe_Picked() {
        // Arrange
        int pickedDiceValues[] = new int[6];
        DiceValue pick;
        
        // Execute
        for (int i = 0; i < 100_000; i++) {
            pick = DiceValue.getRandom(); // From Main.java
            
            pickedDiceValues[pick.ordinal()]++;
        }
        
        // Assert
        for (int i = 0; i < 6; i++) {
            if (pickedDiceValues[i] == 0) {
                fail("The dice value " + DiceValue.values()[i].name() + " was never picked in 100,000 tries");
            }
        }
    }
    
    
    @Test
    public void allPossibleDiceValues_ShouldBe_Rolled() {
        // Arrange
        Dice dice1 = new Dice();
        Dice dice2 = new Dice();
        Dice dice3 = new Dice();
        Player player = new Player("Fred", 100);
        DiceValue randomPick = DiceValue.getRandom();
        int bet = 0;
        Game sut = new Game(dice1, dice2, dice3);
        int roundsPlayed = 0;
        int rolledDiceValues[] = new int[6];
        
        // Execute
        for (/*blank*/; roundsPlayed < 1000; roundsPlayed++) {
            sut.playRound(player, randomPick, bet);
            
            rolledDiceValues[dice1.getValue().ordinal()]++;
            rolledDiceValues[dice2.getValue().ordinal()]++;
            rolledDiceValues[dice3.getValue().ordinal()]++;
        }
        
        // Assert
        for (int i = 0; i < 6; i++) {
            if (rolledDiceValues[i] == 0) {
                fail("The dice value " + DiceValue.values()[i].name() + " was never rolled in 1000 rounds");
            }
        }
    }
}
