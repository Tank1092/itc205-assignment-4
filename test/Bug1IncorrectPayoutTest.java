import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class Bug1IncorrectPayoutTest {

    private DiceValue pick_ = DiceValue.ANCHOR;
    
    @Spy
    private Dice dice_;
    
    @InjectMocks
    private Game sut_;
    
    @Before
    public void setUp() throws Exception {
        when(dice_.getValue()).thenReturn(pick_);
    }

    @Test
    public void playerBalanceAfterRoundShoudBe_ExpectedValue() throws Exception {
        // Arrange
        Player player = new Player("Fred", 100);
        int bet = 5;
        double expectedBalance = player.getBalance() + 15;
        
        // Execute
        sut_.playRound(player, pick_, bet);
        double actualBalance = player.getBalance();
        
        // Assert
        verify(dice_, times(3)).getValue();
        assertThat(actualBalance, equalTo(expectedBalance));
    }
}
